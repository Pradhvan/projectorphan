# Project_Orphan
    A web application to manage less maintained projects. 

## Installation 

1.Create a new directory before cloning the project. 

2.In the same  directory set a vrtual enviornment:
    
    2.1 python3 -m venv venv
    2.2 source venv/bin/activate

3.Clone the repo in the same directory

4.Install Requirements:
    
    pip3 install -r requirements.txt

5.Run the server inside virtual-environment

    python3 manage.py runserver

Now, go to 127.0.0.1:8000/, your welcome page should show up.
